import { MenuItem } from "primeng/api";

const itemHome : MenuItem = {
    icon: 'pi pi-home',
    routerLink: '/aplicaciones'
}

const sexos = [
    {
        label: 'Masculino',
        value: 'M'
    },
    {
        label: 'Femenino',
        value: 'F'
    }
]

export {
    itemHome,
    sexos
}