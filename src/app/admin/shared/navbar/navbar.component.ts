import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { MenuItem } from 'primeng/api';
import { AuthService } from 'src/app/services/auth.service';
import { environment } from 'src/environments/environment.development';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent {

  showSidebar: boolean = false;
  env = environment;
  currentUser: any = null;
  showCerrarSesion: boolean = false;
  sidebarItems: MenuItem[] = [
    {
      icon: 'pi pi-star',
      label: 'Aplicaciones',
      command: () => {
        this.router.navigate(['/aplicaciones']);
        this.showSidebar = false;
      }
    },
  ]

  constructor(private router: Router, private authService: AuthService){}


  logout = () => {
    this.authService.clearUser();
    localStorage.removeItem('token');
    this.router.navigate(['/login']);
  }
}
