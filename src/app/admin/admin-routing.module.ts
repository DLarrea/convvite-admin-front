import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdminComponent } from './admin.component';
import { AplicacionListComponent } from './aplicacion-list/aplicacion-list.component';
import { PersonaListComponent } from './persona-list/persona-list.component';

const routes: Routes = [
  {
    path: '',
    component: AdminComponent,
    children: [
      {
        path: '',
        redirectTo: 'aplicaciones',
        pathMatch: 'full'
      },
      {
        path: 'aplicaciones',
        component: AplicacionListComponent
      },
      {
        path: 'aplicaciones/:id/invitados',
        component: PersonaListComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
