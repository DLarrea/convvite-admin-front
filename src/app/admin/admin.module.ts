import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AdminRoutingModule } from './admin-routing.module';
import { AdminComponent } from './admin.component';
import { AplicacionListComponent } from './aplicacion-list/aplicacion-list.component';
import { PersonaListComponent } from './persona-list/persona-list.component';
import { NavbarComponent } from './shared/navbar/navbar.component';
import { AdminPrimengModule } from '../primeng/admin-primeng/admin-primeng.module';
import { ReactiveFormsModule } from '@angular/forms';
import { ConfirmationService, MessageService } from 'primeng/api';


@NgModule({
  declarations: [
    AdminComponent,
    AplicacionListComponent,
    PersonaListComponent,
    NavbarComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    AdminPrimengModule,
    AdminRoutingModule
  ],
  providers: [
    ConfirmationService,
    MessageService
  ]
})
export class AdminModule { }
