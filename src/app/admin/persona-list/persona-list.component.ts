import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { ConfirmationService, MenuItem, MessageService } from 'primeng/api';
import { Response } from 'src/app/utils/response';
import { fieldValidator, formValidator } from 'src/app/utils/formValidator';
import { itemHome, sexos } from 'src/globals';
import { TipoInvitacionService } from 'src/app/services/tipo-invitacion.service';
import { InvitacionService } from 'src/app/services/invitacion.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-persona-list',
  templateUrl: './persona-list.component.html',
  styleUrls: ['./persona-list.component.scss']
})
export class PersonaListComponent {
  list: any[] = [];
  formAdd : FormGroup;
  formEdit : FormGroup;
  appId: any = null;
  elementDelete: any = null;
  elementAddShow: boolean = false;
  elementEditShow: boolean = false;
  elementAddLoading: boolean = false;
  elementEditLoading: boolean = false;
  formValidator = formValidator;
  fieldValidator = fieldValidator;
  items: MenuItem[] = [];
  home: MenuItem = itemHome;
  sexos = sexos;
  tiposInvitacionList: any = [];

  constructor(private invitacionService: InvitacionService, private formBuilder: FormBuilder, private messageService: MessageService, 
    private confirmationService: ConfirmationService, private loader: NgxUiLoaderService, private tipoInvitacionService: TipoInvitacionService,
    private route:ActivatedRoute) {
    this.formAdd = this.formBuilder.group({
      aplicacion: [null, [Validators.required]],
      nombre: [null, [Validators.required]],
      invitaciones: this.formBuilder.array([])
    });
    this.formEdit = this.formBuilder.group({
      persona_id: [null, [Validators.required]],
      nombre: [null, [Validators.required]],
      aplicacion: [null, [Validators.required]],
      invitaciones: this.formBuilder.array([])
    });

    this.appId = this.route.snapshot.paramMap.get('id');
    this.formAdd.controls['aplicacion'].setValue(this.appId);

    this.route.queryParams.subscribe((data:any) => {
      this.items = [...this.items, { label : data.app }]
    })
  }

  ngOnInit(): void {
    this.get();
    this.getTiposInvitacion();
  }

  get = () => {
    this.loader.start();
    this.invitacionService.get(this.appId).subscribe({
      next: (response: Response) => {
        this.list = response.result;
        this.loader.stop();
      },
      error: (err:HttpErrorResponse) => {
        this.list = [];
        this.loader.stop();
      }
    })
  }

  getTiposInvitacion = () => {
    this.loader.start();
    this.tipoInvitacionService.get().subscribe({
      next: (response: Response) => {
        this.tiposInvitacionList = response.result;
        this.loader.stop();
      },
      error: (err:HttpErrorResponse) => {
        this.tiposInvitacionList = [];
        this.loader.stop();
      }
    })
  }
  
  add = () => {
    if(this.formAdd.valid){
      this.elementAddLoading = true;
      this.invitacionService.post(this.formAdd.value).subscribe({
        next: (response: Response) => {
          this.get();
          this.messageService.add({key: 'admin', severity:'success', summary:'Éxito', detail: 'Elemento agregado'});
          this.elementAddShow = false;
          this.elementAddLoading = false;
          this.formAdd.reset();
          this.formAdd.controls['aplicacion'].setValue(this.appId);
          this.formAdd.controls['invitaciones'].setValue(this.formBuilder.array([]));
        },
        error: (err:HttpErrorResponse) => {
          let error: Response = err.error;
          this.elementAddLoading = false;
          if(Array.isArray(error.message)){
            error.message.forEach(m => {
              this.messageService.add({key: 'admin', severity:'error', summary:'Error', detail: m?.msg || 'Error en el servidor'});
            })
          } else {
            this.messageService.add({key: 'admin', severity:'error', summary:'Error', detail: error.message || 'Error en el servidor'});
          }
        }
      })
    }
  }

  edit = () => {
    if(this.formEdit.valid){
      this.elementEditLoading = true;
      this.invitacionService.put(this.formEdit.value).subscribe({
        next: (response: Response) => {
          this.get();
          this.messageService.add({key: 'admin', severity:'success', summary:'Éxito', detail: 'Elemento editado'});
          this.elementEditShow = false;
          this.elementEditLoading = false;
          this.formEdit.reset();
        },
        error: (err:HttpErrorResponse) => {
          let error: Response = err.error;
          this.elementEditLoading = false;
          if(Array.isArray(error.message)){
            error.message.forEach(m => {
              this.messageService.add({key: 'admin', severity:'error', summary:'Error', detail: m?.msg || 'Error en el servidor'});
            })
          } else {
            this.messageService.add({key: 'admin', severity:'error', summary:'Error', detail: error.message || 'Error en el servidor'});
          }
        }
      })
    }
  }

  delete = (id:number) => {
    this.invitacionService.delete(id).subscribe({
      next: (response: Response) => {
        this.get();
      }, 
      error: (err:HttpErrorResponse) => {
        let error: Response = err.error;
        if(Array.isArray(error.message)){
          error.message.forEach(m => {
            this.messageService.add({key: 'admin', severity:'error', summary:'Error', detail: m?.msg || 'Error en el servidor'});
          })
        } else {
          this.messageService.add({key: 'admin', severity:'error', summary:'Error', detail: error.message || 'Error en el servidor'});
        }
      }
    })
  } 

  setEdit(element:any){
    this.formEdit.reset();
    this.formEdit.controls['persona_id'].setValue(element.persona_id);
    this.formEdit.controls['nombre'].setValue(element.nombre);
    this.formEdit.controls['aplicacion'].setValue(this.appId);
    
    if(Array.isArray(element.invitaciones)){
      for(let invi of element.invitaciones){
        const invitacion = this.formBuilder.group({
          tipo: [null, [Validators.required]],
          cantidad: [null, [Validators.required]]
        });
        invitacion.controls['tipo'].setValue(invi.tipo_invitacion_id);
        invitacion.controls['cantidad'].setValue(invi.cantidad);
        this.invitacionesEdit.push(invitacion)
      }
    }

    this.elementEditShow = true;
  }

  confirm(event: Event, element:any) {
    this.confirmationService.confirm({
      key: 'admin',
      target: event.target ? event.target : undefined,
      message: '¿Eliminar elemento?',
      icon: 'pi pi-exclamation-triangle',
      acceptLabel: 'Si',
      accept: () => {
        this.delete(element.persona_id);
      }
    });
  }

  agregarInvitacion(edit:boolean = false) {
    const invitacion = this.formBuilder.group({
      tipo: [null, [Validators.required]],
      cantidad: [null, [Validators.required]]
    });

    edit ? this.invitacionesEdit.push(invitacion) : this.invitaciones.push(invitacion);
  }

  eliminarInvitacion(index: number, edit:boolean = false) {
    edit ? this.invitacionesEdit.removeAt(index) : this.invitaciones.removeAt(index);
  }

  get invitaciones() {
    return this.formAdd.get('invitaciones') as FormArray;
  }
  
  get invitacionesEdit() {
    return this.formEdit.get('invitaciones') as FormArray;
  }
  
}
