import { TestBed } from '@angular/core/testing';

import { TipoInvitacionService } from './tipo-invitacion.service';

describe('TipoInvitacionService', () => {
  let service: TipoInvitacionService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TipoInvitacionService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
