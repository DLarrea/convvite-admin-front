import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment.development';


@Injectable({
  providedIn: 'root'
})
export class InvitacionService {

  constructor(private http: HttpClient) { }

  get = (id?:number):Observable<Response | any> => {
    return this.http.get<Response | any>(id ? `${environment.api}/invitacion/${id}` : `${environment.api}/invitacion`);
  }

  post = (form:any):Observable<Response | any> => {
    return this.http.post<Response | any>(`${environment.api}/invitacion`, form);
  }

  put = (form:any):Observable<Response | any> => {
    return this.http.put<Response | any>(`${environment.api}/invitacion/${form.persona_id}`, form);
  }

  delete = (id:number):Observable<Response | any> => {
    return this.http.delete<Response | any>(`${environment.api}/invitacion/${id}`);
  }

}
