import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { environment } from 'src/environments/environment.development';
import { Response } from '../utils/response';
import jwt_decode from "jwt-decode";
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private userSource = new BehaviorSubject<any>(null);
  public user = this.userSource.asObservable();

  constructor(private http:HttpClient, private router: Router) { }

  login = (form:any):Observable<Response> => {
    return this.http.post<Response>(`${environment.api}/login`, form);
  }

  setUser = () => {
    if(this.userSource.getValue() == null){
      let token: any = jwt_decode(localStorage.getItem('token') as string);
      if(token.data !== null){
        this.userSource.next(token.data);
      } else {
        this.router.navigate(['/login'])
      }
    }
  }
  
  getUser = () => {
    this.setUser();
    return this.user;
  }

  clearUser = () => {
    this.userSource.next(null);
  }
}
